<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Buku extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }
    function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $buku = $this->db->get('buku')->result();
        } else {
            $this->db->where('id', $id);
            $buku = $this->db->get('buku')->result();
        }
        $this->response($buku, 200);
    }

    function index_post() {
        $data = array(
                    'id'           => $this->post('id'),
                    'judul'          => $this->post('judul'),
                    'deskripsi'    => $this->post('deskripsi'));
        $insert = $this->db->insert('buku', $data);
        if ($insert) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

}
?>